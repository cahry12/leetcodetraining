#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAX_SIZE 100
#ifndef DEBUG
#define DEBUG
#define dbg_print(s) printf("DEBUG MESSAGE:%s\n",s); 
#endif

int cToi(char c) {  
      switch(c) {  
        case 'I': return 1;  
        case 'V': return 5;  
        case 'X': return 10;  
        case 'L': return 50;  
        case 'C': return 100;  
        case 'D': return 500;  
        case 'M': return 1000;  
        default: return 0;  
      }  
    } 
int romanToInt(char* s) {
    int sum=0,i;
    for(i=0;i<strlen(s);i++){
        if( i>0 && cToi(s[i-1]) < cToi(s[i]) ){
            sum+= (cToi(s[i])-2*cToi(s[i-1]));
        } else {
            sum+= cToi(s[i]);
        }
    }
    return sum;
}
int main(int argc,char *argv[])
{
	char str[MAX_SIZE];
	puts("Please input roman symbol:");
	scanf("%s",str);
	printf("Roman symbol %s convert to integer %d\n",str,romanToInt(str));
	return 0;
}
