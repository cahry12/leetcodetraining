#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAX_SIZE 100
#define BAD_VERSION_IDX 95
#ifndef DEBUG
#define DEBUG
#define dbg_print(s) printf("DEBUG MESSAGE:%s\n",s); 
#endif
char product[MAX_SIZE];
int isBadVersion(int version){
	if(product[version]=='N')return 1;
	return 0;
}
int firstBadVersion(int n) {
    int begin=1,end=n;
    int middle;
    while(begin<=end){
	    middle=((end-begin)>>1)+begin;
            if(isBadVersion(middle))end=middle-1;
            else begin=middle+1;
    }
    return end+1;
}
int main(int argc,char *argv[])
{
	memset(product,'Y',MAX_SIZE);
	for(int idx=BAD_VERSION_IDX;idx<MAX_SIZE;idx++)
		product[idx]='N';
	printf("The first index with bad version in product elements is:%d\n",firstBadVersion(MAX_SIZE));
	return 0;
}
