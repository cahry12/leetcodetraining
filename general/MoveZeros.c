#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#ifndef DEBUG
#define DEBUG
#endif
#define SWAP(x,y) { x = x + y; y = x - y; x = x - y; }
#define PRINT_ELE(NUM,MAX_SIZE) {int i=0;\
	for(i=0;i<MAX_SIZE;i++){ \
		printf("%3d",NUM[i]);	\
	} \
	puts("");}
void moveZeroes(int* nums, int numsSize) {
    int start_idx,new_start_idx,total_non_zero=0;
    for(start_idx=0;start_idx<=numsSize-1;start_idx++)
        if(nums[start_idx]!=0)total_non_zero++;
    if(total_non_zero==0)return;
    for(start_idx=0,new_start_idx=0;new_start_idx<=(numsSize-(numsSize-total_non_zero)) && new_start_idx<numsSize;new_start_idx++){
        if(nums[new_start_idx]==0){
            while(nums[start_idx]==0 && start_idx<=numsSize-1){
                start_idx++;
            }
            if(start_idx>numsSize-1)break;
            nums[new_start_idx]=nums[start_idx];
            nums[start_idx++]=0;
        } else {
            start_idx++;
        }
    }
}

int main(int argc, char *argv[])
{
	int max_size;
	printf("Input the size of initial array:");
	scanf("%d",&max_size);
	int num[max_size];
	printf("Input your integer data[Totally %d elements]:",max_size);
	for(int i=0;i<max_size;i++)
		scanf("%d",&num[i]);
	printf("Before move Zeros...\n");
	PRINT_ELE(num,max_size);
	moveZeroes(num,max_size);
	printf("After move Zeros...\n");
	PRINT_ELE(num,max_size);
	return 0;
}
