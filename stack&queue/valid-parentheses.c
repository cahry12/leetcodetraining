#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define SIZE 100
int isValid(char* s) {
    if(s==NULL)exit(1);
    char symbol[SIZE];
    int top=-1;
    int i;
    for(i=strlen(s)-1;i>=0;i--){
        if(s[i]==')'|| s[i]=='}' || s[i]==']')symbol[++top]=s[i];
        else if(s[i]=='(' || s[i]== '{' || s[i]=='['){
            if((s[i]=='(' && symbol[top]!=')') || (s[i]=='[' && symbol[top]!=']') || (s[i]=='{' && symbol[top]!='}'))return 0;
            else {
                symbol[top--]=0;
            }
        }
    }
    if(top!=-1)return 0;
    return 1;
}
int main(int argc,char *argv[])
{
	char str[SIZE];
	puts("Input your string to test whether symbol is a pair");
	scanf("%s",str);
	printf("Is a pair?(y/n):%c\n",isValid(str)==1 ? 'y' : 'n');
	return 0;
}
